
const express = require("express");

// Mongoose is a package that allows creating of Schemas to our model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// Conneting to MongoDB Atlas
// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
mongoose.connect("mongodb+srv://jjjaaalex:admin123@zuitt-bootcamp.xv5ohke.mongodb.net/s35Activity?retryWrites=true&w=majority",
	{	
		// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
		// allows us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Allows to handle errors when the initial connection is established

// Works with the on and once Mongoose methods
let db = mongoose.connection;
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));
// If the connection is successful, out in the console
db.once("open", () => console.log("We're connected to the cloud database"));
// Conneting to MongoDB Atlas END

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Mongoose Schemas
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	name: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value
		default: "pending"
	}
});




/// Activity 35 schema
const signupSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	username: String,
	password: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value
		default: "pending"
	}
});


// [SECTION] Mongoose Models
// Uses schemas and are used to create/instantiate objects that correspond to the schema
// Models use Schemas and they act as the middleman from the server (JS code) to our database

// The variable/object "Task"can now used to run commands for interacting with our database
// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema);

// activity 35 model
const Signup = mongoose.model("Signup", signupSchema);


// Creation of Task Application
// Create a new task
/*
BUSINESS LOGIC
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
3. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) => {
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria as a single object.
	// .then() is chained to another method that is able to return a value/result or an error.
	Task.findOne({name: req.body.name}).then((result, err) => {
		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.name == req.body.name){
			// Return a message to the client/Postman
			return res.send("Duplicate task found");
		  // If no document was found
		} else {
			// Create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});
			// The "save" method will store the information to the database
			// the .save() method returns the result first and the error second as parameters.
			newTask.save().then((savedTask, saveErr) => {
				// If there are errors in saving
				if(saveErr){
					// saveErr is an error object that will contain details about the error
					return console.error(saveErr);
				  // No error found while creating the document
				} else {
					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New task created!");
				}
			})
		}
	})
});


// Getting all the tasks
/*
BUSINESS LOGIC
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req, res) => {
	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}).then((result, err) => {
		// If an error occurred
		if(err) {
			// Will print any errors found in the console
			return console.log(err);
		  // If no errors are found
		} else {
			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			return res.status(200).json({
				data: result
			})
		}
	})
});


/// activity 35



app.post("/signup", (req, res) => {
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria as a single object.
	// .then() is chained to another method that is able to return a value/result or an error.
	Signup.findOne({username: req.body.username}).then((result, err) => {
		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.username == req.body.username){
			// Return a message to the client/Postman
			return res.send("Username already existed");
		  // If no document was found
		} else {
			// Create a new task and save it to the database
			let newSignup = new Signup({
				username: req.body.username,
				password: req.body.password
			});
			// The "save" method will store the information to the database
			// the .save() method returns the result first and the error second as parameters.
			newSignup.save().then((savedSignup, saveErr) => {
				// If there are errors in saving
				if(saveErr){
					// saveErr is an error object that will contain details about the error
					return console.error(saveErr);
				  // No error found while creating the document
				} else {
					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New user registered ");
				}
			})
		}
	})
});


// Getting all the tasks
/*
BUSINESS LOGIC
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
/*
app.get("/tasks", (req, res) => {
	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}).then((result, err) => {
		// If an error occurred
		if(err) {
			// Will print any errors found in the console
			return console.log(err);
		  // If no errors are found
		} else {
			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			return res.status(200).json({
				data: result
			})
		}
	})
});



*/





app.listen(port, () => console.log(`Server running at port ${port}`));

